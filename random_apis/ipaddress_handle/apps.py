from django.apps import AppConfig


class IpaddressHandleConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ipaddress_handle"
