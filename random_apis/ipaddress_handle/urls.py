from django.urls import path
from . import views

urlpatterns = [
    path('api/getipaddress', views.GetIpAddress.as_view()),
]
