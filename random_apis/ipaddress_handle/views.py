from django.shortcuts import render
from rest_framework.response import Response
from rest_framework import generics, status
from rest_framework.views import APIView
import requests, json

# Create your views here.

class GetIpAddress(APIView):

    def get(self, request, *args, **kwargs):
        res = requests.get("https://wtfismyip.com/json")
        if res.status_code == 200:
            resp = json.loads(res.text)
            reply = {
                "Yourip":resp["YourFuckingIPAddress"],
                "Yourlocation":resp["YourFuckingLocation"],
                "YourISP":resp["YourFuckingISP"],
                "YourCity":resp["YourFuckingCity"],
                "YourCountry":resp["YourFuckingCountry"]
            }
            return Response({"data":reply})
        else:
            return Response({"message":"Please try again later or contact admin."})




